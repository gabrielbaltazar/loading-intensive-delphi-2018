unit uNotificacao;

interface

type TModelNotification = class(TInterfacedObject)
  private
    FText: String;
    FDetailText: String;
    procedure SetText(const Value: String);
    procedure SetDetailText(const Value: String);

  public
    property text: String read FText write SetText;
    property detailText: String read FDetailText write SetDetailText;

    constructor create; overload;
    constructor create(AText: String; ADetailText: String = ''); overload;

end;

implementation

{ TModelNotification }

constructor TModelNotification.create(AText, ADetailText: String);
begin
  Self.FText          := AText;
  Self.FDetailText := ADetailText;
end;

constructor TModelNotification.create;
begin

end;

procedure TModelNotification.SetText(const Value: String);
begin
  FText := Value;
end;

procedure TModelNotification.SetDetailText(const Value: String);
begin
  FDetailText := Value;
end;

end.
