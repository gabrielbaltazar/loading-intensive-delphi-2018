unit uNotification;

interface

type TNotification = class
  private
    FText: String;
    FDetailText: String;
    Fposition: Integer;
    FmaxPosition: Integer;
    procedure SetText(const Value: String);
    procedure SetDetailText(const Value: String);
    procedure SetmaxPosition(const Value: Integer);
    procedure Setposition(const Value: Integer);

  public
    property position: Integer read Fposition write Setposition;
    property maxPosition: Integer read FmaxPosition write SetmaxPosition;
    property text: String read FText write SetText;
    property detailText: String read FDetailText write SetDetailText;

    constructor create; overload;
    constructor create(AText: String; ADetailText: String = ''; AMaxPosition: Integer = 0; APosition: Integer = 0); overload;

end;

implementation

{ TNotification }

constructor TNotification.create;
begin

end;

procedure TNotification.SetText(const Value: String);
begin
  FText := Value;
end;

constructor TNotification.create(AText, ADetailText: String; AMaxPosition, APosition: Integer);
begin
  Self.FText       := AText;
  Self.FDetailText := ADetailText;
  Self.Fposition   := APosition;
  Self.FmaxPosition:= AMaxPosition;
end;

procedure TNotification.SetDetailText(const Value: String);
begin
  FDetailText := Value;
end;

procedure TNotification.SetmaxPosition(const Value: Integer);
begin
  FmaxPosition := Value;
end;

procedure TNotification.Setposition(const Value: Integer);
begin
  Fposition := Value;
end;

end.
