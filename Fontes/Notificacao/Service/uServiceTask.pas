unit uServiceTask;

interface

uses IServiceNotification, System.Threading, FLoading, Forms, System.SysUtils, System.Classes,
     FProgress;

type TExecEmTask = reference to procedure (ASrvNotification: ISrvNotification = nil);

type TServiceTask = class

  private
    class procedure executar(AProc: TExecEmTask; ASrvNotification: ISrvNotification);

  public
    class procedure executaComLoading (AProc: TExecEmTask);
    class procedure executaComProgress(AProc: TExecEmTask);

end;

implementation

{ TServiceTask }

class procedure TServiceTask.executaComLoading(AProc: TExecEmTask);
begin
  frmLoading := TfrmLoading.Create(Application);
  executar(AProc, frmLoading);
end;

class procedure TServiceTask.executaComProgress(AProc: TExecEmTask);
begin
  frmProgress := TfrmProgress.Create(Application);
  executar(AProc, frmProgress);
end;

class procedure TServiceTask.executar(AProc: TExecEmTask; ASrvNotification: ISrvNotification);
var
  task: ITask;
begin
  ASrvNotification.show;

  task := TTask.Create(
    procedure()
    begin
      try
        AProc(ASrvNotification);
      finally
        ASrvNotification.Close;
      end;
    end
  );

  task.Start;
end;

end.
