unit IServiceNotification;

interface

uses uNotification, System.SysUtils;

type ISrvNotification = interface ['{F86CC089-74BF-4341-BF2E-79A982BE5321}']

  procedure notificar (ANotification: TNotification);
  procedure show;
  procedure close;

end;

implementation

end.
