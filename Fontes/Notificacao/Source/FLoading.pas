unit FLoading;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.WinXCtrls, Vcl.StdCtrls, IServiceNotification,
  uNotification, System.Threading, Vcl.ExtCtrls;

type
  TfrmLoading = class(TForm, ISrvNotification)
    actLoading: TActivityIndicator;
    lblText: TLabel;
    lblDetailText: TLabel;
    Shape1: TShape;
  private
    { Private declarations }
  public
    procedure notificar (ANotification: TNotification);

    { Public declarations }
  end;

var
  frmLoading: TfrmLoading;

implementation

{$R *.dfm}

{ TfrmLoading }

procedure TfrmLoading.notificar(ANotification: TNotification);
begin
  TThread.Queue(nil, procedure
    begin
      lblText.Caption       := ANotification.text;
      lblDetailText.Caption := ANotification.detailText;
    end
  );
end;

end.
