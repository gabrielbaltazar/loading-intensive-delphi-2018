unit FProgress;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls, IServiceNotification,
  Vcl.ExtCtrls, uNotification;

type
  TfrmProgress = class(TForm, ISrvNotification)
    Shape1: TShape;
    lblText: TLabel;
    lblDetailText: TLabel;
    progressBar: TProgressBar;
  private
    { Private declarations }
  public
    procedure notificar (ANotification: TNotification);
    { Public declarations }
  end;

var
  frmProgress: TfrmProgress;

implementation

{$R *.dfm}

{ TfrmProgress }

procedure TfrmProgress.notificar(ANotification: TNotification);
begin
  TThread.Queue(nil, procedure
    begin
      progressBar.Max       := ANotification.maxPosition;
      progressBar.Position  := ANotification.position;
      lblText.Caption       := ANotification.text;
      lblDetailText.Caption := ANotification.detailText;
    end
  );
end;

end.
