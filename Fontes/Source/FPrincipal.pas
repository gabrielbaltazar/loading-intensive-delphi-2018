unit FPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uNotification, Data.DB, IServiceNotification,
  Vcl.StdCtrls, Datasnap.DBClient, Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, System.Threading,
  Vcl.Imaging.jpeg, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MySQL, FireDAC.Phys.MySQLDef,
  FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.Provider,
  uPessoaController, uServiceTask;

type
  TfrmPrincipal = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    btnExportar: TButton;
    FDConn: TFDConnection;
    fdqPessoa: TFDQuery;
    btnInserir: TButton;
    dsPessoa: TDataSource;
    btnLimpar: TButton;
    btnCarregar: TButton;
    Panel2: TPanel;
    btnExportarTask: TButton;
    btnInserirTask: TButton;
    Button3: TButton;
    btnCarregarTask: TButton;
    Label1: TLabel;
    Label2: TLabel;
    btnSair: TButton;
    fdqPessoaidpessoa: TFDAutoIncField;
    fdqPessoanome: TStringField;
    fdqPessoalogradouro: TStringField;
    fdqPessoaemail: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnCarregarClick(Sender: TObject);
    procedure btnInserirClick(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
    procedure btnCarregarTaskClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnInserirTaskClick(Sender: TObject);
    procedure btnExportarTaskClick(Sender: TObject);
  private
    _Controller: TPessoaController;

    { Private declarations }
    procedure carregar(ASrvNotification: ISrvNotification = nil);
    procedure exportar(ASrvNotification: ISrvNotification = nil);
    procedure inserir (ASrvNotification: ISrvNotification = nil);
 
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;


implementation

{$R *.dfm}

procedure TfrmPrincipal.btnCarregarClick(Sender: TObject);
begin
  carregar(nil);
end;

procedure TfrmPrincipal.btnCarregarTaskClick(Sender: TObject);
begin
  TServiceTask.executaComLoading(carregar);
end;

procedure TfrmPrincipal.btnExportarClick(Sender: TObject);
begin
  exportar(nil);
end;

procedure TfrmPrincipal.btnExportarTaskClick(Sender: TObject);
begin
  TServiceTask.executaComProgress(exportar);
end;

procedure TfrmPrincipal.btnInserirClick(Sender: TObject);
begin
  inserir(nil);
end;

procedure TfrmPrincipal.btnInserirTaskClick(Sender: TObject);
begin
  TServiceTask.executaComProgress(inserir);
end;

procedure TfrmPrincipal.btnLimparClick(Sender: TObject);
begin
  fdqPessoa.Active   := False;
  fdqPessoa.SQL.Text := 'DELETE FROM PESSOA';
  fdqPessoa.ExecSQL;
  carregar;
end;

procedure TfrmPrincipal.btnSairClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmPrincipal.carregar(ASrvNotification: ISrvNotification);
begin
  fdqPessoa.Active   := False;
  fdqPessoa.SQL.Text := 'SELECT * FROM PESSOA';
  _Controller.carregar(fdqPessoa, ASrvNotification);
end;

procedure TfrmPrincipal.exportar(ASrvNotification: ISrvNotification);
begin
  _Controller.exportar(fdqPessoa, ASrvNotification);
end;

procedure TfrmPrincipal.FormCreate(Sender: TObject);
begin
  _Controller := TPessoaController.Create;
end;

procedure TfrmPrincipal.FormDestroy(Sender: TObject);
begin
  FreeAndNil(_Controller);
end;

procedure TfrmPrincipal.inserir(ASrvNotification: ISrvNotification);
begin
  _Controller.inserir(fdqPessoa, ASrvNotification);
end;

end.
