object frmPrincipal: TfrmPrincipal
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = 'frmPrincipal'
  ClientHeight = 438
  ClientWidth = 893
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 356
    Width = 893
    Height = 41
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 396
    ExplicitWidth = 746
    object Label2: TLabel
      AlignWithMargins = True
      Left = 344
      Top = 4
      Width = 106
      Height = 33
      Margins.Left = 15
      Align = alLeft
      Caption = 'EXECUTAR SEM TASK'
      Layout = tlCenter
    end
    object btnExportar: TButton
      Left = 249
      Top = 1
      Width = 80
      Height = 39
      Align = alLeft
      Caption = 'Exportar'
      TabOrder = 0
      OnClick = btnExportarClick
    end
    object btnInserir: TButton
      Left = 81
      Top = 1
      Width = 80
      Height = 39
      Align = alLeft
      Caption = 'Inserir'
      TabOrder = 1
      OnClick = btnInserirClick
      ExplicitLeft = 169
    end
    object btnLimpar: TButton
      Left = 1
      Top = 1
      Width = 80
      Height = 39
      Align = alLeft
      Caption = 'Limpar'
      TabOrder = 2
      OnClick = btnLimparClick
      ExplicitLeft = 0
      ExplicitTop = -3
    end
    object btnCarregar: TButton
      Left = 161
      Top = 1
      Width = 88
      Height = 39
      Align = alLeft
      Caption = 'Carregar'
      TabOrder = 3
      OnClick = btnCarregarClick
      ExplicitLeft = 175
      ExplicitTop = 6
    end
    object btnSair: TButton
      Left = 808
      Top = 1
      Width = 84
      Height = 39
      Align = alRight
      Caption = 'Sair'
      TabOrder = 4
      OnClick = btnSairClick
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 893
    Height = 356
    Align = alClient
    DataSource = dsPessoa
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Panel2: TPanel
    Left = 0
    Top = 397
    Width = 893
    Height = 41
    Align = alBottom
    TabOrder = 2
    ExplicitLeft = 8
    ExplicitTop = 420
    object Label1: TLabel
      AlignWithMargins = True
      Left = 344
      Top = 4
      Width = 106
      Height = 33
      Margins.Left = 15
      Align = alLeft
      Caption = 'EXECUTAR COM TASK'
      Layout = tlCenter
      ExplicitHeight = 13
    end
    object btnExportarTask: TButton
      Left = 249
      Top = 1
      Width = 80
      Height = 39
      Align = alLeft
      Caption = 'Exportar'
      TabOrder = 0
      OnClick = btnExportarTaskClick
    end
    object btnInserirTask: TButton
      Left = 81
      Top = 1
      Width = 80
      Height = 39
      Align = alLeft
      Caption = 'Inserir'
      TabOrder = 1
      OnClick = btnInserirTaskClick
      ExplicitLeft = 49
      ExplicitTop = 5
    end
    object Button3: TButton
      Left = 1
      Top = 1
      Width = 80
      Height = 39
      Align = alLeft
      Caption = 'Limpar'
      TabOrder = 2
      OnClick = btnLimparClick
      ExplicitLeft = -15
      ExplicitTop = 6
    end
    object btnCarregarTask: TButton
      Left = 161
      Top = 1
      Width = 88
      Height = 39
      Align = alLeft
      Caption = 'Carregar'
      TabOrder = 3
      OnClick = btnCarregarTaskClick
      ExplicitLeft = 1
    end
  end
  object FDConn: TFDConnection
    Params.Strings = (
      'Database=DelphiIntensive2018'
      'User_Name=root'
      'Password=root'
      'Server=localhost'
      'DriverID=MySQL')
    Connected = True
    LoginPrompt = False
    Left = 144
    Top = 56
  end
  object fdqPessoa: TFDQuery
    Active = True
    Connection = FDConn
    SQL.Strings = (
      'select * from pessoa')
    Left = 152
    Top = 112
    object fdqPessoaidpessoa: TFDAutoIncField
      DisplayWidth = 10
      FieldName = 'idpessoa'
      Origin = 'idpessoa'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object fdqPessoanome: TStringField
      AutoGenerateValue = arDefault
      DisplayWidth = 28
      FieldName = 'nome'
      Origin = 'nome'
      Size = 100
    end
    object fdqPessoalogradouro: TStringField
      AutoGenerateValue = arDefault
      DisplayWidth = 33
      FieldName = 'logradouro'
      Origin = 'logradouro'
      Size = 100
    end
    object fdqPessoaemail: TStringField
      AutoGenerateValue = arDefault
      DisplayWidth = 34
      FieldName = 'email'
      Origin = 'email'
      Size = 100
    end
  end
  object dsPessoa: TDataSource
    DataSet = fdqPessoa
    Left = 152
    Top = 160
  end
end
