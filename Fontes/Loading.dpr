program Loading;

uses
  Vcl.Forms,
  FPrincipal in 'Source\FPrincipal.pas' {frmPrincipal},
  IServiceNotification in 'Notificacao\Service\IServiceNotification.pas',
  uNotification in 'Notificacao\Model\uNotification.pas',
  uServiceTask in 'Notificacao\Service\uServiceTask.pas',
  uPessoaController in 'Controller\uPessoaController.pas',
  FLoading in 'Notificacao\Source\FLoading.pas' {frmLoading},
  FProgress in 'Notificacao\Source\FProgress.pas' {frmProgress};

{$R *.res}

begin
  Application.Initialize;
  ReportMemoryLeaksOnShutdown   := True;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.Run;
end.
