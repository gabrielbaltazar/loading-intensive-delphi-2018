unit uPessoaController;

interface

uses Datasnap.DBClient, System.Classes, System.SysUtils, IServiceNotification, uNotification, Data.DB;

type TPessoaController = class

  private
    procedure notificar(ANotificacao: TNotification; ASrvNotification : ISrvNotification);

  public
    procedure inserir (ADataSet: TDataSet; ASrvNotification: ISrvNotification = nil);
    procedure carregar(ADataSet: TDataSet; ASrvNotification: ISrvNotification = nil);
    procedure exportar(ADataSet: TDataSet; ASrvNotification: ISrvNotification = nil);

end;

implementation

{ TPessoaController }

procedure TPessoaController.carregar(ADataSet: TDataSet; ASrvNotification: ISrvNotification);
var
  notificacao: TNotification;
begin
  notificacao := TNotification.create('Aguarde...', 'Carregando Dados Pessoa');
  notificar(notificacao, ASrvNotification);
  ADataSet.Open;
  Sleep(3000);
  FreeAndNil(notificacao);
end;

procedure TPessoaController.exportar(ADataSet: TDataSet; ASrvNotification: ISrvNotification);
var
  arquivo    : TStringList;
  notificacao: TNotification;
begin
  arquivo     := TStringList.Create;
  notificacao := TNotification.create('Exportando Dados', '', ADataSet.RecordCount);

  ADataSet.First;
  while not ADataSet.Eof do
  begin
//    Sleep(500);
    notificacao.position   := ADataSet.RecNo;
    notificacao.detailText := 'Exportando Pessoa ' + notificacao.position.ToString + ' de ' + notificacao.maxPosition.ToString;
    notificar(notificacao, ASrvNotification);

    arquivo.Add(ADataSet.FieldByName('NOME').AsString  + ';' +
                ADataSet.FieldByName('EMAIL').AsString + ';' +
                ADataSet.FieldByName('LOGRADOURO').AsString);

    ADataSet.Next;
  end;

  arquivo.SaveToFile('pessoa.txt');
  FreeAndNil(arquivo);
  FreeAndNil(notificacao);
end;

procedure TPessoaController.inserir(ADataSet: TDataSet; ASrvNotification: ISrvNotification);
var
  i     : Integer;
  numero: Integer;
  total: Integer;
  notificacao: TNotification;
  nome       : String;
begin
  total       := 5000;
  notificacao := TNotification.create('Inserindo Dados', '', total);

  for I := 1 to total do
  begin
    notificacao.position   := i;
    notificacao.detailText := 'Inserindo Pessoa ' + notificacao.position.ToString + ' de ' + notificacao.maxPosition.ToString;
    notificar(notificacao, ASrvNotification);

    numero := Random(total);
    ADataset.Append;
    ADataSet.FieldByName('NOME').AsString       := 'PESSOA ' + i.ToString + ' ' + numero.ToString;
    ADataSet.FieldByName('EMAIL').AsString      := ADataSet.FieldByName('NOME').AsString + '@gmail.com'.ToLower;
    ADataSet.FieldByName('LOGRADOURO').AsString := 'ENDERE�O ' + ADataSEt.FieldByName('NOME').AsString;
    ADataSet.Post;

  end;

  FreeAndNil(notificacao);
end;

procedure TPessoaController.notificar(ANotificacao: TNotification; ASrvNotification: ISrvNotification);
begin
  if Assigned(ASrvNotification) then
    ASrvNotification.notificar(ANotificacao);
end;

end.
